// import {Flight} from '@/models/flight'
import {FileReaderWithPromise} from '@/models/fileReaderWithPromise'
import {Flight} from '@/models/flight'

export class FlightCollection {

	constructor() {
		this.flights = []
	}

	async loadFromCSV(file) {
		const fileReader = new FileReaderWithPromise(file)
		return new Promise(async (resolve, reject) => {
			try {
				const response = await fileReader.read(file)
				this.processCSVData(response)
				resolve(response)
			} catch(error) {
				reject(error)
			}
		})
	}

	processCSVData(data) {

		this.flights = []

		// Get rid of header
		const rows = data.split('\n').splice(1)
		
		const flightData = []
		rows.forEach(row => {
			flightData.push(row.split(','))
		})

		let flightDestinations = rows.map(row => {
			const columns = row.split(',')
			return columns[1]
		})
		let uniqueFlightDestinations =  [...new Set(flightDestinations)]

		uniqueFlightDestinations.forEach(flightDestination => {
			let rowsForDestination = flightData.filter(flightDataRow => (flightDataRow[1] === flightDestination))
			let flight = new Flight(rowsForDestination[0][3], rowsForDestination[0][2], rowsForDestination[0][1])
			flight.populateSchedules(rowsForDestination)
			this.flights.push(flight)
		})
	}

	getUniqueDestinations() {
		const destinations = this.flights.map(flight => (flight.destinationText))
		return [...new Set(destinations)]
	}
	getFlatArray(filters) {
		let flights = []
		if(filters.destination) {
			flights = this.flights.filter(flight => flight.destinationText === filters.destination)
		} else {
			flights = this.flights
		}
		let outputArray = []
		flights.forEach(flight => {
			let schedules = []
			if(filters.date) {
				schedules = flight.getSchedulesForDate(filters.date)
			} else {
				schedules = flight.schedules
			}
			schedules.forEach(schedule => {
				outputArray.push({
					flightNumber: flight.flightNumber,
					destinationCode: flight.destinationCode,
					destinationText: flight.destinationText,
					time: schedule.time
				})
			})
		})
		const moment = require('moment')
		outputArray.sort((a,b) => {
			let date1 = moment().startOf('day')
			date1.hours(a.time.split(':')[0])
			date1.minutes(a.time.split(':')[1])
			let date2 = moment().startOf('day')
			date2.hours(b.time.split(':')[0])
			date2.minutes(b.time.split(':')[1])

			if(date1.isBefore(date2)) {
				return -1
			}
			if(date1.isAfter(date2)) {
				return 1
			}
			if(date1.isSame(date2)) {
				return 0
			}
		})
		return outputArray
	}

}

export default FlightCollection