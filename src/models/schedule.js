export class Schedule {

	constructor(time) {
		this.time = time
		// Array storing days flight runs, Sunday 0, Saturday 6
		this.daysRunning = [
			false,
			false,
			false,
			false,
			false,
			false,
			false
		]
	}

	populateDaysRunning(rowData) {
		for(let i = 4; i <= 10; i++) {
			this.daysRunning[i - 4] = rowData[i] === 'x'
		}
	}

}

export default Schedule