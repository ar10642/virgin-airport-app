import {Schedule} from '@/models/schedule.js'

export class Flight {

	constructor(flightNumber, destinationCode, destinationText) {
		this.flightNumber = flightNumber
		this.destinationText = destinationText
		this.destinationCode = destinationCode
		this.schedules = []
	}

	populateSchedules(rowData) {
		this.schedules = []
		rowData.forEach(row => {
			let schedule = new Schedule(row[0])
			schedule.populateDaysRunning(row)
			this.schedules.push(schedule)
		})
	}
	getSchedulesForDate(date) {
		return this.schedules.filter(schedule => schedule.daysRunning[date.getDay()])
	}

}

export default Flight