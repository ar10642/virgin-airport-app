export class FileReaderWithPromise {
	
	constructor(file) {
		this.file = file
	}

	read() {
		return new Promise((resolve, reject) => {
			const reader = new FileReader()
			try {
				reader.readAsText(this.file)
				reader.onload = (event) => {
					resolve(event.target.result)
				}
			} catch(error) {
				reject(error)
			}
		})
	}

}