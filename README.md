# virgin-airport-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

There is a pre-built version of the test in the dist folder.

It assumes the URL you'll be using is http://localhost/virgin-airport-app/dist

If you want to change this, modify the **baseUrl** property in vue.config.js and re-run **npm run build**.

If not working, I would recommend using **npm_run_serve** and going to http://localhost:8080

You'll need an up to date version of node.js and npm to do this.